import numpy as np
from joblib import Parallel

from sklearn.cluster._mean_shift import estimate_bandwidth
from sklearn.base import BaseEstimator, ClusterMixin
from sklearn.neighbors import NearestNeighbors, KDTree


def TMeanShift():

    def __init__(self, X, sn_target):
        self.X = X
        self.sn_target = sn_target
        self.tree = KDTree(self.X[:, :2])

    def _sn_in_cell(self, x, y, radius):
        """
        Compute the combined  S/N of a set of pixels within
        a radius r of the position x, y
        """

        i_nbrs = self.tree.query_radius([[x, y]], r=radius, return_distance=False, count_only=False)
        total_signal = np.sum(self.X[i_nbrs, 2])
        total_noise = np.sqrt(np.sum(self.X[i_nbrs, 3] ** 2))
        return total_signal / total_noise

    def _tmean_shift_single_seed(self, my_mean, max_iter, min_radius):
        """
        X has strictly 4 columns: x, y, signal, noise
        neighbors are defined by euclidean distance in 2D space (x, y)
        the mean is the S/N-weighted centroid
        """
        # For each seed, climb gradient until convergence or max_iter
#        bandwidth = nbrs.get_params()["radius"]
#        stop_thresh = 1e-3 * bandwidth  # when mean has converged
#        completed_iterations = 0

        x, y = my_mean
        my_sn = self._sn_in_cell(x, y, min_radius)
        while True:
            # Find mean of points within bandwidth
            points_within = X[i_nbrs]
            if np.abs(my_sn - sn)
                break  # Depending on seeding strategy this condition may occur
            my_old_mean = my_mean  # save the old mean
            my_mean = np.mean(points_within, axis=0)
            # If converged or at max_iter, adds the cluster
            if (
                np.linalg.norm(my_mean - my_old_mean) < stop_thresh
                or completed_iterations == max_iter
            ):
                break
            completed_iterations += 1
        return tuple(my_mean), len(points_within), completed_iterations





