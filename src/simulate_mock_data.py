import numpy as np
from astropy.io import fits
from astropy.modeling import models
from astropy.convolution import convolve_fft, convolve
from matplotlib import pyplot as plt
from matplotlib import colors
from vorbin.voronoi_2d_binning import voronoi_2d_binning
rng = np.random.default_rng(42)

@models.custom_model
def ExpDiskFaceOn(x, y, h_R=2.25, amplitude=1):
    """
    Face-on expontential disk model
    """
    r = np.hypot(x, y)
    return amplitude * np.exp(-r / h_R)

@models.custom_model
def SpiralArmsFaceOn(x, y, pitch=np.pi/8, scale=6, arm_width=0.4, pairs=2, theta=0, amplitude=1, dim_scale=3):
    """
    Exponential spiral formula
    """
    r = np.hypot(x, y)
    k = np.tan(pitch)
    rs = np.zeros_like(r)
    pairs = int(pairs)
    out = np.zeros_like(r)
    for i in range(pairs):
        phi = np.arctan2(y, x) + theta + 2 * np.pi * i / pairs
        for j in range(int(np.pi / pitch)):
            phi += np.pi * j * (-1) ** j
            rs = scale * np.exp(k * phi)
            out += amplitude * np.exp(-(r - rs) ** 2 / (2 * arm_width ** 2))
    return out * np.exp(-r / dim_scale)

def two_gaussians_with_noisy_stripes():
    shape = 100, 100
    noise_map = np.ones(shape)

    noise_map[:, 50:54] = 2 # vertical stripe
    noise_map[42:46, :] = 3 # horizontal stripe

    model = models.Gaussian2D(
        x_mean = 40,
        y_mean = 40,
        x_stddev = 15,
        y_stddev = 7,
        amplitude = 7,
        theta = 40,
    ) + \
    models.Gaussian2D(
        x_mean = 50,
        y_mean = 60,
        x_stddev = 7,
        y_stddev = 10,
        amplitude = 11,
        theta = 30,
    )

    xcoord1d = np.arange(shape[0])
    ycoord1d = np.arange(shape[1])
    x, y = np.meshgrid(xcoord1d, ycoord1d)
    model_evaluated = model(x, y)

    # Add point sources and holes
    model_evaluated[50, 60] = 125
    model_evaluated[40, 44] = 290/2
    model_evaluated[70, 40] = 150


    psf_model = models.Gaussian2D(1, 0, 0, x_stddev=1, y_stddev=1)
    psfcoord1d = np.arange(11) - 11 // 2
    xpsf, ypsf = np.meshgrid(psfcoord1d, psfcoord1d)
    psf = psf_model(xpsf, ypsf)
    psf *= 1 / np.sum(psf)
    model_evaluated_convolved = convolve(model_evaluated, psf)

    noise = rng.normal(loc=0, scale=noise_map)
    model_evaluated_noisy = model_evaluated_convolved + noise
    noise_hdu = fits.ImageHDU(data=noise_map)
    data_hdu = fits.ImageHDU(data=model_evaluated_noisy)
    psf_hdu = fits.ImageHDU(data=psf)
    model_hdu = fits.ImageHDU(data=model_evaluated_convolved)
    hdul = fits.HDUList([fits.PrimaryHDU(), data_hdu, noise_hdu, psf_hdu, model_hdu])
    hdul.writeto('../data/test/two_gaussians_with_noisy_stripes.fits', overwrite=True)

def faceon_spiral():
    # Face-on grid
    x, y = np.meshgrid(np.linspace(-20, 20, 101), np.linspace(-20, 20, 101))
    noise_map = np.hypot(x, y) * 0.05


    # Bulge
    bulge_reff = 4.5
    bulge_amp = 4
    bulge_ellip = 0.0
    bulge_theta = np.pi/4


    bulge_face_on = models.Sersic2D(r_eff=bulge_reff, amplitude=bulge_amp, n=4, ellip=bulge_ellip, theta=bulge_theta)
    noise_map += np.sqrt(bulge_face_on(x, y))
    # Disk
    amp_thin = 9
    h_R = 7.25
    thin_disk_face_on = ExpDiskFaceOn(h_R=h_R, amplitude=amp_thin)

    # Spiral arms
    arm_width=0.8
    spiral_amplitude = 12
    pitch = np.pi/6
    pairs= 3
    spiral_scale = 6
    spiral_arms = SpiralArmsFaceOn(theta=bulge_theta, pairs=pairs, arm_width=arm_width, pitch=pitch,
                                   amplitude=spiral_amplitude, scale=spiral_scale, dim_scale=h_R)
    full_face_on = bulge_face_on(x, y) + thin_disk_face_on(x, y) + spiral_arms(x, y)
    noise = rng.normal(loc=0, scale=noise_map)

    psf_model = models.Gaussian2D(1, 0, 0, x_stddev=1, y_stddev=1)
    psfcoord1d = np.arange(11) - 11 // 2
    xpsf, ypsf = np.meshgrid(psfcoord1d, psfcoord1d)
    psf = psf_model(xpsf, ypsf)
    psf *= 1 / np.sum(psf)
    model_evaluated_convolved = convolve(full_face_on, psf)

    noise = rng.normal(loc=0, scale=noise_map)
    model_evaluated_noisy = model_evaluated_convolved + noise
    noise_hdu = fits.ImageHDU(data=noise_map)
    data_hdu = fits.ImageHDU(data=model_evaluated_noisy)
    psf_hdu = fits.ImageHDU(data=psf)
    model_hdu = fits.ImageHDU(data=model_evaluated_convolved)
    hdul = fits.HDUList([fits.PrimaryHDU(), data_hdu, noise_hdu, psf_hdu, model_hdu])
    hdul.writeto('../data/test/faceon_spiral.fits', overwrite=True)

if __name__ == '__main__':
    faceon_spiral()




