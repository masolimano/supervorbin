import sys
import numpy as np
from scipy.spatial import Voronoi, voronoi_plot_2d
from matplotlib import pyplot as plt

eps = sys.float_info.epsilon

class VoronoiChromosome(Voronoi):
    """
    Class based on SciPy's Voronoi but adapted for evolving the generators with
    a genetic algorithm
    """
    def __init__(self, points, furthest_site=False, incremental=False, qhull_options=None, bounding_box=None):
        """
        The first addition to SciPy's class is the ability to receive a rectangular bounding for the points. This
        required for having only closed regions in the tessellation (in the vanilla version the boundaries go to infinity.

        Note: this function draws heavily from user Flabetvibes answer to https://stackoverflow.com/q/28665491
        """
        self.bounding_box = bounding_box

        if bounding_box is not None:
            i = np.logical_and(np.logical_and(bounding_box[0] <= points[:, 0],
                                         points[:, 0] <= bounding_box[1]),
                          np.logical_and(bounding_box[2] <= points[:, 1],
                                        points[:, 1] <= bounding_box[3]))
            ##  Mirror points
            points_center = points[i, :]
            points_left = np.copy(points_center)
            points_left[:, 0] = bounding_box[0] - (points_left[:, 0] - bounding_box[0])
            points_right = np.copy(points_center)
            points_right[:, 0] = bounding_box[1] + (bounding_box[1] - points_right[:, 0])
            points_down = np.copy(points_center)
            points_down[:, 1] = bounding_box[2] - (points_down[:, 1] - bounding_box[2])
            points_up = np.copy(points_center)
            points_up[:, 1] = bounding_box[3] + (bounding_box[3] - points_up[:, 1])
            points_new = np.append(points_center,
                               np.append(np.append(points_left,
                                                   points_right,
                                                   axis=0),
                                         np.append(points_down,
                                                   points_up,
                                                   axis=0),
                                         axis=0),
                               axis=0)

            # Instanciate from parent class using the updated list of sites
            super().__init__(
                points_new,
                furthest_site = furthest_site,
                incremental = incremental,
                qhull_options = qhull_options
            )

            # Filter regions
            regions = []
            for region in self.regions:
                flag = True
                for index in region:
                    if index == -1:
                        flag = False
                        break
                    else:
                        x = self.vertices[index, 0]
                        y = self.vertices[index, 1]
                        if not(bounding_box[0] - eps <= x and x <= bounding_box[1] + eps and
                               bounding_box[2] - eps <= y and y <= bounding_box[3] + eps):
                            flag = False
                            break
                if region != [] and flag:
                    regions.append(region)
            self.filtered_points = points_center
            self.filtered_regions = regions

        else:
            super().__init__(
                points,
                furthest_site = furthest_site,
                incremental = incremental,
                qhull_options = qhull_options
            )


    def get_centroids(self):
        vertices = self.vertices
        # Polygon's signed area
        A = 0
        # Centroid's x
        C_x = 0
        # Centroid's y
        C_y = 0
        for i in range(0, len(vertices) - 1):
            s = (vertices[i, 0] * vertices[i + 1, 1] - vertices[i + 1, 0] * vertices[i, 1])
            A = A + s
            C_x = C_x + (vertices[i, 0] + vertices[i + 1, 0]) * s
            C_y = C_y + (vertices[i, 1] + vertices[i + 1, 1]) * s
        A = 0.5 * A
        C_x = (1.0 / (6.0 * A)) * C_x
        C_y = (1.0 / (6.0 * A)) * C_y
        return np.array([[C_x, C_y]])



    def _compute_sn_of_regions(self, X):
        pass

if __name__ == '__main__':
    rng = np.random.default_rng(42)
    npoints = 100
    sites = rng.uniform(low=0, high=1, size=(npoints, 2))
    fig, ax = plt.subplots()
    bounding_box = np.array([0., 1., 0., 1.]) # [x_min, x_max, y_min, y_max]

    vor = VoronoiChromosome(sites, bounding_box=bounding_box)
    ax.plot(vor.filtered_points[:, 0], vor.filtered_points[:, 1], 'b.')

    # Plot ridges points
    for region in vor.filtered_regions:
        vertices = vor.vertices[region, :]
        ax.plot(vertices[:, 0], vertices[:, 1], 'go')
    # Plot ridges
    for region in vor.filtered_regions:
        vertices = vor.vertices[region + [region[0]], :]
        ax.plot(vertices[:, 0], vertices[:, 1], 'k-')

    plt.show()
