import os
import numpy as np
from astropy.io import fits
from matplotlib import pyplot as plt
from astropy import visualization as vis
plt.style.use('ticky')
from vorbin.voronoi_2d_binning import voronoi_2d_binning

if __name__ == '__main__':
    name = 'faceon_spiral'
    outpath = f'../output/vorbin_example/{name}'
    if not os.path.exists(outpath):
        os.mkdir(outpath)


    hdul = fits.open(f'../data/test/{name}.fits')
    x, y = np.meshgrid(np.arange(101), np.arange(101))
    w = np.where(hdul[4].data > 0)
    x = x[w]
    y = y[w]
    signal = hdul[1].data[w]
    noise = hdul[2].data[w]

    sn_map = hdul[4].data / hdul[2].data

    if not os.path.exists(f'{outpath}/data_and_sn_map.pdf'):
        fig, ax = plt.subplots(ncols=2, sharex=True, sharey=True, figsize=(10, 4))
        norm = vis.ImageNormalize(hdul[1].data, interval=vis.PercentileInterval(99))
        im0 = ax[0].imshow(hdul[1].data, origin='lower', norm=norm)
        ax[0].set_title('Data')
        cax0 = fig.colorbar(im0, orientation='vertical', pad=0, ax=ax[0])
        im1 = ax[1].imshow(sn_map, origin='lower', norm=vis.ImageNormalize(stretch=vis.SqrtStretch(), vmax=20), cmap='plasma')
        ax[1].set_title('S/N map')
        cax1 = fig.colorbar(im1, orientation='vertical', pad=0, ax=ax[1])
        plt.tight_layout()
        fig.savefig(f'{outpath}/data_and_sn_map.pdf', bbox_inches='tight')
        fig.savefig(f'{outpath}/data_and_sn_map.png', bbox_inches='tight')
        plt.close(fig)

    target_sn=10

    bin_number, x_gen, y_gen, x_bar, y_bar, sn, nPixels, scale = voronoi_2d_binning(
    x, y, signal, noise, target_sn, pixelsize=1, plot=True, quiet=False)
    fig = plt.gcf()
    fig.set_figheight(9)
    fig.set_figwidth(5)
    fig.savefig(f'{outpath}/vorbin_result_sn{target_sn:.0f}.pdf', bbox_inches='tight')
    fig.savefig(f'{outpath}/vorbin_result_sn{target_sn:.0f}.png', bbox_inches='tight')
    plt.close(fig)

    bin_map = bin_number.reshape(101, 101)
    fits.writeto(f'{outpath}/bin_map_sn{target_sn:.0f}.fits', data=bin_map, overwrite=True)

    nbins = np.unique(bin_number).size
    print(f'Number of bins = {nbins}')
    mean_flux = np.zeros_like(bin_map)
    mean_flux = mean_flux.astype(np.float64)
    for k in range(nbins):
        wherebin = np.where(bin_map == k)
        mean_flux[wherebin] = np.nanmean(hdul[1].data[wherebin])

    fits.writeto(f'{outpath}/binned_flux_map_sn{target_sn:.0f}.fits', data=mean_flux, overwrite=True)


    fig, ax = plt.subplots()
    im3 = ax.imshow(mean_flux, origin='lower', norm=vis.ImageNormalize(mean_flux, interval=vis.PercentileInterval(97)))
    fig.colorbar(im3, ax=ax, label='Mean flux (arbitrary units)', orientation='vertical')
    fig.savefig(f'{outpath}/vorbin_mean_flux_sn{target_sn:.0f}.pdf', bbox_inches='tight')
    fig.savefig(f'{outpath}/vorbin_mean_flux__sn{target_sn:.0f}.png', bbox_inches='tight')
    plt.close(fig)

